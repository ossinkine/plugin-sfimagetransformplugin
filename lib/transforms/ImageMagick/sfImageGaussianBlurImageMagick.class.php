<?php

class sfImageGaussianBlurImageMagick extends sfImageTransformAbstract
{
  protected $value;

  public function __construct($value)
  {
    $this->setValue($value);
  }

  /**
   * @param int $value
   */
  public function setValue($value)
  {
    $this->value = $value;
  }

  /**
   * @return int
   */
  public function getValue()
  {
    return $this->value;
  }

  protected function transform(sfImage $image)
  {
    $resource = $image->getAdapter()->getHolder();

    $resource->gaussianBlurImage(0, $this->getValue(), Imagick::CHANNEL_ALL);

    return $image;
  }
}
